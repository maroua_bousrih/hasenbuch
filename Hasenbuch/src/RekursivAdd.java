
public class RekursivAdd
{

	public static void main(String[] args)
	{
		System.out.println(add(1)); // =======> 1 

		System.out.println(add(2));// =========> 3 

		System.out.println(add(3));// ===========> 6

		System.out.println(add(4));//==========> 10
	}
	
	public static int add(int zahl)
	{
		 if(zahl == 0)
		 {
			 return 0;
		 }
		 
		int result=	zahl;
		zahl--;
		result = result + add(zahl);
		return (result);
	}
}
